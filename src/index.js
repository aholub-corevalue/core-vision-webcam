'use strict';

function standardDeviation(values) {
  var avg = average(values);

  var squareDiffs = values.map(function (value) {
    var diff = value - avg;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });

  var avgSquareDiff = average(squareDiffs);

  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data) {
  var sum = data.reduce(function (sum, value) {
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}

$(function () {
  var ctx,
    interval = null,
    period = 200,
    frameCounter = 0,
    historySize = 40,
    allUsers = [],
    width = 640,
    height = 480,
    data = null,
    collected = [],
    isStreamAllowed = true,
    syncStream = true,
    similarUsersContainer = document.getElementById('similar-users'),
    similarUsersChartContainer = document.getElementById('similar-users-chart'),
    historySizeInput = document.getElementById('history-size'),
    usersBlock = document.getElementById('selected-users'),
    colors = ['red', 'blue', 'green', 'pink', 'cian', 'orange'];

  init();

  function init() {
    for (var i = 0; i < 128; i++) collected.push([]);
    initCanvas();
    initCamera();
    initUsers(function (users) {
      allUsers = _.sortBy(users, 'user_name');
    });
  }

  function initCanvas() {
    var canvas = document.getElementById('layer');
    canvas.addEventListener('click', detectUser);
    ctx = canvas.getContext('2d');

    historySizeInput.value = historySize;
    historySizeInput.addEventListener('change', changeHistorySize);
  }

  function initCamera() {
    Webcam.set({
      width: width,
      height: height,
      image_format: 'jpeg',
      jpeg_quality: 70
    });

    Webcam.attach('#my_camera');

    Webcam.on('load', function () {
      console.log('Start stream');
      interval = setInterval(startStream, period);
    });

    document.addEventListener('keyup', (event) => {
      if (event.key === '+') {
        if (period === 10) period = 0;
        period += 100;
      } else if (event.key === '-') {
        period -= 100;
        if (period < 10) period = 10;
      }
      clearInterval(interval);
      interval = setInterval(startStream, period);

      console.log('Stream frame period set: ' + period);
    });
  }

  function fakeGUID() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  function startStream() {
    if (isStreamAllowed && syncStream) {
      syncStream = false;
      Webcam.snap(function (data_uri) {
        const payload = {
          command: 1,
          image: data_uri,
          frame_id: fakeGUID()
        };

        var myRequest = new Request('/api', {method: 'POST', body: JSON.stringify(payload)});

        fetch(myRequest).then(function (response) {
          if (response.status === 200) {
            frameCounter++;
            response.json().then(function (res) {
              if (res) {
                if (isStreamAllowed) updateTracking(res);
              }
              syncStream = true;
            });
          } else {
            throw new Error('Something went wrong on api server!');
          }
        }).catch(function (error) {
          console.error(error);
          syncStream = true;
        });
      });
    }
  }

  function addEmbedding(emb) {
    for (var i in emb) {
      collected[i].push(emb[i]);
      if (collected[i].length > historySize) collected[i].shift()
    }
  }

  function changeHistorySize(evt) {
    var oldSize = collected[0].length;
    var newSize = parseInt(this.value);
    if (oldSize > newSize) {
      for (var i = 0; i < 128; i++) {
        collected[i].splice(0, oldSize - newSize);
      }
    }
    historySize = newSize;
  }

  function updateTracking(response) {
    ctx.fillStyle = 'rgba(225,225,225,1.0)';
    ctx.clearRect(0, 0, width, height);

    if (!('data' in response)) return;
    data = response.data;

    displayUsers(data.users);

    for (var i = 0; i < data.embeddings.length; i++) {
      displayBarChart(data.embeddings[i], data.users[i], similarUsersChartContainer, colors[i]);
      break;
    }

    for (var i = 0; i < data.positions.length; i++) {
      var locationData = data.positions[i];
      var pos = locationData.location;

      ctx.beginPath();
      ctx.strokeSize = '1';

      ctx.fillStyle = 'rgba(0,255,0,1.0)';
      ctx.font = '14px Arial';
      ctx.fillStyle = 'rgba(255,255,225,1.0)';
      ctx.fillText(frameCounter, 0, 14);

      if ('points' in locationData.location) {
        var points = locationData.location.points;
        // for (var j = 0; j < points.length; j++) {
        //   ctx.moveTo(points[j].x + 5-j, points[j].y);
        //   ctx.arc(points[j].x, points[j].y, 3, 0, 2 * Math.PI, false);
        // }
        ctx.moveTo(points[0].x + 3, points[0].y);
        ctx.arc(points[0].x, points[0].y, 3, 0, 2 * Math.PI, false);
        ctx.moveTo(points[1].x + 3, points[1].y);
        ctx.arc(points[1].x, points[1].y, 3, 0, 2 * Math.PI, false);
        ctx.moveTo(points[2].x + 2, points[2].y);
        ctx.arc(points[2].x, points[2].y, 2, 0, 2 * Math.PI, false);
        ctx.moveTo(points[3].x + 1, points[3].y);
        ctx.arc(points[3].x, points[3].y, 1, 0, 2 * Math.PI, false);
        ctx.moveTo(points[4].x + 1, points[4].y);
        ctx.arc(points[4].x, points[4].y, 1, 0, 2 * Math.PI, false);
      }

      ctx.strokeStyle = colors[i];
      ctx.rect(pos.x, pos.y, pos.w, pos.h);
      ctx.stroke();
      ctx.closePath();
    }
  }

  function displayUsers(users) {
    if (users && users.length) {
      similarUsersContainer.innerHTML = '';
      for (var i = 0; i < users.length; i++) {
        var similarUsers = users[i];
        if (similarUsers) {
          var ol = document.createElement('ol');
          ol.style.borderColor = colors[i];
          for (var j = 0; j < similarUsers.length; j++) {
            var similarUser = similarUsers[j];
            var li = document.createElement('li');
            li.innerText = similarUser.user_name + ' ' + similarUser.dist.toFixed(2);
            if (similarUser.learning) {
              li.style.color = colors[i];
              var sessionsBlock = usersBlock.querySelector('#u' + similarUser.user_id.replace(':', '_') + ' .embeddings-counts');
              if (sessionsBlock) sessionsBlock.innerText =
                'Embeddings confirmed: ' + similarUser['counts']['confirmed']
                + ', unconfirmed: ' + similarUser['counts']['unconfirmed']
                + ', current: ' + similarUser['counts']['current'];
            }
            ol.appendChild(li);
          }
          similarUsersContainer.appendChild(ol);
        }
      }
    }
  }

  function displayBarChart(embeddingsData, foundUser, placeholder, color) {
    addEmbedding(embeddingsData);

    var id = 'c0';
    var chartCanvas = document.getElementById(id);
    if (!chartCanvas) {
      chartCanvas = document.createElement('canvas');
      chartCanvas.id = id;
      chartCanvas.classList.add('bar-chart');
      chartCanvas.width = 384;
      chartCanvas.height = 400;
      placeholder.appendChild(chartCanvas);
    }

    var ctx2 = chartCanvas.getContext('2d');
    ctx2.fillStyle = 'rgba(225,225,225,1.0)';
    ctx2.clearRect(0, 0, 384, 400);

    ctx2.beginPath();
    ctx2.strokeStyle = 'rgba(0, 255, 0, 1.0)';
    ctx2.moveTo(0, 200);
    ctx2.lineTo(384, 200);
    ctx2.closePath();
    ctx2.stroke();

    for (var i = 0; i < 128; i++) {
      var bar = collected[i];

      var a = average(bar);
      var sigma = standardDeviation(bar);
      ctx2.beginPath();
      ctx2.fillStyle = 'rgba(255, 0, 0, 0.044)';
      ctx2.fillRect(i * 3, 200 - (a + 3 * sigma) * 500, 3, sigma * 6 * 500);
      ctx2.closePath();
      ctx2.beginPath();
      ctx2.fillStyle = 'rgba(255, 0, 0, 0.238)';
      ctx2.fillRect(i * 3, 200 - (a + 2 * sigma) * 500, 3, sigma * 4 * 500);
      ctx2.closePath();
      ctx2.beginPath();
      ctx2.fillStyle = 'rgba(255, 0, 0, 0.563)';
      ctx2.fillRect(i * 3, 200 - (a + 1 * sigma) * 500, 3, sigma * 2 * 500);
      ctx2.closePath();
      ctx2.fill();

      ctx2.beginPath();
      ctx2.strokeStyle = 'rgba(0, 0, 0, 1.0)';
      ctx2.moveTo(i * 3, 200 - a * 500);
      ctx2.lineTo(i * 3 + 3, 200 - a * 500);
      ctx2.closePath();
      ctx2.stroke();
    }
  }

  function detectUser(src, evt) {
    var rect = src.target.getBoundingClientRect();
    var pos = {
      x: src.clientX - rect.left,
      y: src.clientY - rect.top
    };
    var usersInRectIndex = 0;

    if (data) {
      for (var i = 0; i < data.users.length; i++) {
        var similarUsers = data.users[i];
        if (similarUsers && similarUsers.length) {
          var l = similarUsers[0].position.location;
          if (l.x < pos.x && pos.x < (l.x + l.w) && l.y < pos.y && pos.y < (l.y + l.h)) {
            usersInRectIndex = i;
          }
        }
      }
    }

    isStreamAllowed = false;
    similarUsersContainer.innerHTML = '';

    var otherUsers = _.differenceBy(allUsers, data.users[usersInRectIndex], 'user_id');
    var usersForSelect = data.users[usersInRectIndex].concat(otherUsers);

    var selectList = document.createElement('select');
    selectList.setAttribute('id', 'users-list');

    for (var i = 0; i < usersForSelect.length; i++) {
      var user = usersForSelect[i];
      var option = document.createElement('option');
      option.value = user.user_id;
      option.text = user.user_name;
      if (user.dist) {
        option.text += ' ' + user.dist.toFixed(2);
        option.classList.add('similar');
      }

      selectList.appendChild(option);
    }
    similarUsersContainer.appendChild(selectList);

    var buttonItsMe = document.createElement('input');
    buttonItsMe.type = 'button';
    buttonItsMe.value = 'It\'s me!';
    buttonItsMe.onclick = function () {
      var user = getSelectedUser(usersForSelect);
      sendUser(user, data.embeddings[usersInRectIndex]);
    };
    similarUsersContainer.appendChild(buttonItsMe);

    var buttonCancel = document.createElement('input');
    buttonCancel.type = 'button';
    buttonCancel.value = 'Cancel';
    buttonCancel.onclick = function () {
      isStreamAllowed = true;
    };
    similarUsersContainer.appendChild(buttonCancel);
  }

  function addUserSessionsBlock(user) {
    var id = 'u' + user.user_id.replace(':', '_');
    if (document.getElementById(id) == null) {
      var li = document.createElement('li');
      li.id = id;
      li.innerHTML = '<strong class="user-name">' + user.user_name + '</strong> <small class="embeddings-counts"></small>';
      var buttonStop = document.createElement('input');
      buttonStop.type = 'button';
      buttonStop.value = 'Stop record';
      buttonStop.onclick = function () {
        isStreamAllowed = true;
        sendStopRecordUser(user);
        li.parentNode.removeChild(li);
      };
      li.appendChild(buttonStop);
      usersBlock.appendChild(li);
    }
  }

  function getSelectedUser(users) {
    var selectList = document.getElementById('users-list');
    var userId = selectList.options[selectList.selectedIndex].value;

    var user = null;
    for (var i = 0; i < users.length; i++) {
      if (users[i].user_id === userId) {
        user = users[i];
        break;
      }
    }

    return user;
  }

  function sendUser(user, emb) {
    if (user) {
      var request = new Request('/api/select-user', {
        method: 'POST', body: JSON.stringify({
          user_id: user.user_id,
          emb: emb
        })
      });

      fetch(request).then(function (response) {
        addUserSessionsBlock(user);
        isStreamAllowed = true;
      });
    }
  }

  function sendStopRecordUser(user) {
    if (user) {
      var request = new Request('/api/deselect-user', {
        method: 'POST', body: JSON.stringify({
          user_id: user.user_id,
        })
      });

      fetch(request).then(function (response) {
        isStreamAllowed = true;
      });
    }
  }

  function initUsers(callback) {
    var request = new Request('/api/init-users', {
      method: 'GET'
    });

    fetch(request).then(function (response) {
      if (response.status === 200) {
        response.json().then(function (result) {
          callback(result.users);
        });
      }
    });

  }

});
